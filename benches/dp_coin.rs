use std::collections::HashMap;

use criterion::{criterion_group, criterion_main, Criterion};
use rand::thread_rng;
use rand::{seq::SliceRandom, Rng};
use zlrust::dp::coin::{solve_memo_min_coins_for_target, solve_simple_min_coins_for_target};

fn random_coins_arr() -> Vec<u64> {
    let mut values: Vec<u64> = (0..100).collect();
    values.shuffle(&mut thread_rng());
    values.truncate(10);
    values
}

fn coins_simple(target: i64, coins: &[u64]) {
    solve_simple_min_coins_for_target(target, coins);
}

fn coins_memo(target: i64, coins: &[u64]) {
    solve_memo_min_coins_for_target(target, coins, &mut HashMap::new());
}

fn criterion_benchmark(c: &mut Criterion) {
    let coins = random_coins_arr();
    let target = thread_rng().gen_range(0..50);
    c.bench_function("dp_coins_simple", |b| {
        b.iter(|| coins_simple(target, &coins))
    });
    c.bench_function("dp_coins_memo", |b| b.iter(|| coins_memo(target, &coins)));
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
