use criterion::{criterion_group, criterion_main, Criterion};
use rand::Rng;
use zlrust::unsafe_cast::{vec_u8_to_vec_u32, vec_u8_to_vec_u32_safe};

fn safe_cast(vec: Vec<u8>) -> Vec<u32> {
    vec_u8_to_vec_u32_safe(vec)
}

fn unsafe_cast(vec: Vec<u8>) -> Vec<u32> {
    vec_u8_to_vec_u32(vec)
}

fn criterion_benchmark(c: &mut Criterion) {
    let vec: Vec<u8> = (0..100000).map(|_| rand::thread_rng().gen()).collect();
    let v2 = vec.clone();
    c.bench_function("Safe cast", |b| b.iter(|| safe_cast(v2.clone())));
    c.bench_function("Unsafe cast", |b| b.iter(|| unsafe_cast(vec.clone())));
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
