use criterion::{criterion_group, criterion_main, Criterion};
use zlrust::peak_finding::{peak_finding, peak_finding_trivial};

const ARR: [i32; 34] = [
    1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 9, 8, 7, 6, 5, 4, 3, 2, 1, -1, -2, -3,
    -4, -5, -5, -6, -7, -8,
];

fn bench_peak_find() {
    peak_finding(ARR);
}

fn bench_peak_find_trivial() {
    peak_finding_trivial(ARR);
}

fn criterion_benchmark(c: &mut Criterion) {
    c.bench_function("peak_find", |b| b.iter(bench_peak_find));
    c.bench_function("peak_find_trivial", |b| b.iter(bench_peak_find_trivial));
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
