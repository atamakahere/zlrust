use std::collections::HashMap;

pub fn solve_simple_min_coins_for_target(target: i64, coins: &[u64]) -> i64 {
    match target {
        x if x < 0 => i64::MAX,
        0 => 0,
        x => coins
            .iter()
            .map(|c| solve_simple_min_coins_for_target(x - *c as i64, coins).saturating_add(1))
            .min()
            .unwrap(),
    }
}

pub fn solve_memo_min_coins_for_target(
    target: i64,
    coins: &[u64],
    memo: &mut HashMap<i64, i64>,
) -> i64 {
    if let Some(&cv) = memo.get(&target) {
        return cv;
    }

    let cv = match target {
        x if x < 0 => i64::MAX,
        0 => 0,
        x => coins
            .iter()
            .map(|c| solve_memo_min_coins_for_target(x - *c as i64, coins, memo).saturating_add(1))
            .min()
            .unwrap_or(i64::MAX), // Handle potential None case
    };

    memo.insert(target, cv);

    cv
}

#[cfg(test)]
mod test {
    use crate::dp::coin::{solve_memo_min_coins_for_target, solve_simple_min_coins_for_target};

    #[test]
    fn simple() {
        let coins = [1, 3, 4];
        assert_eq!(2, solve_simple_min_coins_for_target(5, &coins));
    }

    #[test]
    fn memo() {
        let coins = [1, 3, 4];
        assert_eq!(
            2,
            solve_memo_min_coins_for_target(5, &coins, &mut std::collections::HashMap::new())
        );
    }
}
