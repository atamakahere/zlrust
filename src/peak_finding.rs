pub fn peak_finding<E, I>(elements: E) -> Option<usize>
where
    E: AsRef<[I]>,
    I: PartialEq + PartialOrd + Ord,
{
    match elements.as_ref() {
        [] => None,     // No elements
        [_] => Some(0), // Only 1 elements
        [first, second] => (first > second)
            .then_some(0)
            .or((second > first).then_some(1)), // Only 2 elements
        arr => (arr.first() > arr.get(1)) // Is the first element peak?
            .then_some(0)
            // If not, find the peak in middle of the array
            .or(arr
                .windows(3)
                .position(|window| window[1] > window[0] && window[1] >= window[2])
                .map(|pos| pos.saturating_add(1))
                // Didn't find the peak in middle? check the last element
                .or((arr.last() > arr.get(arr.len() - 2)).then_some(arr.len() - 1))),
    }
}

pub fn peak_finding_trivial<E, I>(elements: E) -> Option<usize>
where
    E: AsRef<[I]>,
    I: PartialEq + PartialOrd + Ord,
{
    let arr = elements.as_ref();

    if arr.is_empty() {
        return None;
    }

    if arr.len() == 1 {
        return Some(0);
    }

    // Check if the first element is a peak
    if arr[0] >= arr[1] {
        return Some(0);
    }

    // Check if the last element is a peak
    if arr[arr.len() - 1] >= arr[arr.len() - 2] {
        return Some(arr.len() - 1);
    }

    // Check for peaks in the interior of the array
    (1..arr.len() - 1).find(|&i| arr[i] >= arr[i - 1] && arr[i] >= arr[i + 1])
}

#[cfg(test)]
mod test {
    use crate::peak_finding::peak_finding;
    // use crate::peak_finding::peak_finding_trivial as peak_finding;
    //
    #[test]
    fn peak_find_small() {
        let elements = vec![10];
        assert_eq!(peak_finding(elements), Some(0));

        let elements = vec![10, 20];
        assert_eq!(peak_finding(elements), Some(1));

        let elements = vec![30, 20];
        assert_eq!(peak_finding(elements), Some(0));
    }

    #[test]
    fn peak_find_middle() {
        let elements = vec![10, 50, 30]; // Failing for this
        assert_eq!(peak_finding(elements), Some(1));

        let elements = vec![10, 20, 30, 40, 32, 10];
        assert_eq!(peak_finding(elements), Some(3));

        let elements = vec![10, 20, 30, 40, 40, 40, 32, 10];
        assert_eq!(peak_finding(elements), Some(3));
    }

    #[test]
    fn peak_find_edge() {
        let elements = vec![30, 20, 10];
        assert_eq!(peak_finding(elements), Some(0));

        let elements = vec![10, 20, 30];
        assert_eq!(peak_finding(elements), Some(2));

        let elements = vec![44, 40, 32, 12, 10, 9];
        assert_eq!(peak_finding(elements), Some(0));

        let elements = vec![10, 12, 32, 45, 72, 88];
        assert_eq!(peak_finding(elements), Some(5));
    }

    #[test]
    fn peak_find_none() {
        let elements: Vec<i32> = vec![];
        assert_eq!(peak_finding(elements), None);

        let elements = vec![10, 10, 10, 10, 10, 10, 10];
        assert_eq!(peak_finding(elements), None);

        let elements = vec![10, 10, 10];
        assert_eq!(peak_finding(elements), None);

        let elements = vec![10, 10];
        assert_eq!(peak_finding(elements), None);
    }
}
