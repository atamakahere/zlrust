pub fn vec_u8_to_vec_u32(vec: Vec<u8> /* takes ownership */) -> Vec<u32> {
    assert!(vec.len() % 4 == 0);
    let vec = std::mem::ManuallyDrop::new(vec);
    unsafe { Vec::from_raw_parts(vec.as_ptr() as *mut u32, vec.len() / 4, vec.len() / 4) }
}

pub fn vec_u8_to_vec_u32_safe(vec: Vec<u8> /* takes ownership */) -> Vec<u32> {
    vec.chunks(4)
        .map(|chunk| u32::from_le_bytes(chunk.try_into().unwrap()))
        .collect()
}
